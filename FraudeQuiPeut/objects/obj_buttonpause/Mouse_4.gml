/// @description Insert description here
// You can write your code in this editor

ispaused = !ispaused;

if (ispaused) {
	obj_pausebackground.image_alpha = 1;
}
else if (!ispaused) {
	obj_pausebackground.image_alpha = 0;
}