/// @description Insert description here
// You can write your code in this editor

if (obj_controller.x - x < 0 ) {
	obj_controller.sprite_index = spr_controller_right;
}
else {
	obj_controller.sprite_index = spr_controller_left;
}

alarm[1] = 1 * room_speed;

if (sprite_index == obj_fraudster.sprite_index && obj_buttonpause.ispaused == false) {
	 obj_time.timer = obj_time.timer +1;
	 event_perform_object(obj_fraudster, ev_other, ev_user0);
	 layer_sequence_create("Instance_sequence", 960, 720, sq_2secondes);
	audio_play_sound(snd_rightclick, 20, false);
}
else if (sprite_index != obj_fraudster.sprite_index && obj_buttonpause.ispaused == false) {
    obj_time.timer = obj_time.timer - 10;
	 layer_sequence_create("Instance_sequence", 960, 720, sq_10secondes);
	 	audio_play_sound(snd_wrongclick, 20, false);
}