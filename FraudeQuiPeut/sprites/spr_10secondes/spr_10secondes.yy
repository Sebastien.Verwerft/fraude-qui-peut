{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 279,
  "bbox_top": 0,
  "bbox_bottom": 83,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 280,
  "height": 84,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"788f899a-8f2d-4bab-90f1-d288f531999d","path":"sprites/spr_10secondes/spr_10secondes.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"788f899a-8f2d-4bab-90f1-d288f531999d","path":"sprites/spr_10secondes/spr_10secondes.yy",},"LayerId":{"name":"8083b9c3-d8b9-4a04-bfa6-9eae4882fc68","path":"sprites/spr_10secondes/spr_10secondes.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_10secondes","path":"sprites/spr_10secondes/spr_10secondes.yy",},"resourceVersion":"1.0","name":"788f899a-8f2d-4bab-90f1-d288f531999d","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_10secondes","path":"sprites/spr_10secondes/spr_10secondes.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"91a88b98-0806-48e7-9e61-cbefaae2305e","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"788f899a-8f2d-4bab-90f1-d288f531999d","path":"sprites/spr_10secondes/spr_10secondes.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 140,
    "yorigin": 42,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_10secondes","path":"sprites/spr_10secondes/spr_10secondes.yy",},
    "resourceVersion": "1.3",
    "name": "spr_10secondes",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"8083b9c3-d8b9-4a04-bfa6-9eae4882fc68","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Sprites",
    "path": "folders/Sprites.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_10secondes",
  "tags": [],
  "resourceType": "GMSprite",
}