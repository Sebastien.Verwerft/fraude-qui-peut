{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 217,
  "bbox_right": 1304,
  "bbox_top": 592,
  "bbox_bottom": 3413,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 1600,
  "height": 3418,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"a8ed9646-31d8-4256-b636-2cc2161731fc","path":"sprites/spr_perso1/spr_perso1.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a8ed9646-31d8-4256-b636-2cc2161731fc","path":"sprites/spr_perso1/spr_perso1.yy",},"LayerId":{"name":"56de5276-ebb2-48fd-898d-90f0dd39b5f3","path":"sprites/spr_perso1/spr_perso1.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_perso1","path":"sprites/spr_perso1/spr_perso1.yy",},"resourceVersion":"1.0","name":"a8ed9646-31d8-4256-b636-2cc2161731fc","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_perso1","path":"sprites/spr_perso1/spr_perso1.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"7b5cc0f8-314c-4b1c-8776-18a70a4456cb","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a8ed9646-31d8-4256-b636-2cc2161731fc","path":"sprites/spr_perso1/spr_perso1.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 800,
    "yorigin": 3408,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_perso1","path":"sprites/spr_perso1/spr_perso1.yy",},
    "resourceVersion": "1.3",
    "name": "spr_perso1",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"56de5276-ebb2-48fd-898d-90f0dd39b5f3","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Sprites",
    "path": "folders/Sprites.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_perso1",
  "tags": [],
  "resourceType": "GMSprite",
}